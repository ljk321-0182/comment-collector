评论收集器
=========

收集网上的一些评论，防止将来走失。

支持的商城：

* 华为 (vmall.com)
* 荣耀 (hihoner.com)
* OPPO (opposhop.cn)
* VIVO (shop.vivo.com.cn)

### 构建环境

* Visual Studio 2022
* .NET MAUI

### 声明

本应用收集的数据仅供学习和参考，请勿用于非法用途。

### 协议

```text
do What The Fuck you want to Public License

Version 1.0, March 2000
Copyright (C) 2000 Banlu Kemiyatorn (]d).
136 Nives 7 Jangwattana 14 Laksi Bangkok
Everyone is permitted to copy and distribute verbatim copies
of this license document, but changing it is not allowed.

Ok, the purpose of this license is simple
and you just

DO WHAT THE FUCK YOU WANT TO.
```