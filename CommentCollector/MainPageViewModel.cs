﻿using System.ComponentModel;
using System.Windows.Input;
using System.Globalization;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.Text;

namespace CommentCollector
{
    public class StringLengthToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((string)value).Length > 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BoolNotConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
    }

    internal interface IDialogService
    {
        Task ShowAlertAsync(string title, string message, string accept);

        Task ShowAlertAsync(string title, string message, string accept, string cancel);
    }

    internal class DialogService : IDialogService
    {
        public Task ShowAlertAsync(string title, string message, string accept)
        {
            return Application.Current.MainPage.DisplayAlert(title, message, accept);
        }

        public Task ShowAlertAsync(string title, string message, string accept, string cancel)
        {
            return Application.Current.MainPage.DisplayAlert(title, message, accept, cancel);
        }
    }

    public class MainPageViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string currentUrl;
        bool loading;

        IDialogService dialogService;

        public bool Loading
        {
            get
            {
                return loading;
            }
            set
            {
                if (loading != value)
                {
                    loading = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Loading)));
                }
            }
        }

        public string CurrentUrl
        {
            get
            {
                return currentUrl;
            }
            set
            {
                if (currentUrl != value)
                {
                    currentUrl = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentUrl)));
                }
            }
        }

        public ICommand CollectCommand { get; private set; }

        public ICommand ExportCommand { get; private set; }

        public ObservableCollection<ICommentItem> Comments { get; private set; }

        public MainPageViewModel()
        {
            Comments = new ObservableCollection<ICommentItem>();

            CollectCommand = new Command<string>(async (str) =>
            {
                try
                {
                    Loading = true;
                    var collector = CollectorFactory.ConstructCollector(CurrentUrl);
                    var result = await collector.Collect(CurrentUrl);
                    Comments.Clear();
                    foreach (var item in result)
                    {
                        Comments.Add(item);
                    }
                }
                catch (UriFormatException ex)
                {
                    Loading = false;
                    await dialogService.ShowAlertAsync("错误", "不合法的 URL", "好");
                }
                finally 
                { 
                    Loading = false; 
                }
                }
            );

            ExportCommand = new Command<string>(async (str) =>
            {
                if (Comments.Count == 0)
                {
                    return;
                }

                var sb = new StringBuilder("Id,Content\n");
                foreach (var item in Comments)
                {
                    sb.Append($"{item.Id},{item.Content}\n");
                }

                string targetFile = System.IO.Path.Combine(FileSystem.Current.AppDataDirectory, "data.csv");

                using FileStream outputStream = System.IO.File.OpenWrite(targetFile);
                using StreamWriter streamWriter = new StreamWriter(outputStream);

                await streamWriter.WriteAsync(sb);

                await dialogService.ShowAlertAsync("提示", $"成功导出到 {targetFile}", "好");
            });
            dialogService = new DialogService();
        }
    }

}
