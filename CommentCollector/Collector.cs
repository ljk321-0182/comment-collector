﻿using System.Net;
using System.Net.Http.Json;
using System.Text.Json;

namespace CommentCollector
{
    internal class CollectorFactory
    {
        public static CollectorBase ConstructCollector(string url)
        {
            var uri = new Uri(url);
            if (uri.Host.Contains("vmall.com"))
            {
                return new VmallCollector();
            }
            if (uri.Host.Contains("hihonor.com"))
            {
                return new HonorShopCollector();
            }
            if (uri.Host.Contains("opposhop.cn"))
            {
                return new OppoShopCollector();
            }
            if (uri.Host.Contains("shop.vivo.com.cn"))
            {
                return new VivoShopCollector();
            }

            throw new Exception("Unrecognized URL");
        }
    }

    public interface ICommentItem
    {
        public long Id { get; }

        public string Content { get; }
    }

    internal abstract class CollectorBase
    {
        protected readonly HttpClient _httpClient = new(new HttpClientHandler()
        {
            AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate | DecompressionMethods.Brotli
        });

        protected const int PAGE_SIZE = 10;

        public CollectorBase()
        {
            _httpClient.DefaultRequestHeaders.Add(
                "User-Agent",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0"
                );
        }

        public virtual async Task<ICollection<ICommentItem>> Collect(string url)
        {
            await HttpGet(url);
            _httpClient.DefaultRequestHeaders.Add("Referer", url);
            var productId = CommonExtractProductId(url);
            if (productId > 0)
            {
                return await GetComment(productId);
            }
            return new List<ICommentItem>();
        }

        protected async Task<HttpResponseMessage> HttpGet(string url)
        {
            return await _httpClient.GetAsync(url);
        }

        protected async Task<HttpResponseMessage> HttpPost(string url, object body)
        {
            return await _httpClient.PostAsJsonAsync(url, body);
        }

        protected static long CommonExtractProductId(string url)
        {
            var uri = new Uri(url);
            var path = uri.AbsolutePath;
            var parts = path.Split('/');
            var last = parts.Last();
            if (long.TryParse(last.Split('.').First(), out var productId))
            {
                return productId;
            }

            return -1;
        }

        protected async Task<ICollection<ICommentItem>> GetComment(long productId)
        {
            int pageNum = 1;
            var result = new List<ICommentItem>();
            while (result.Count < 100)
            {
                var curPage = await GetCommentByPage(productId, pageNum);
                if (curPage.Count == 0)
                {
                    break;
                }
                foreach (var item in curPage)
                {
                    // Filter empty comment
                    if (item.Content != "用户未填写评价内容")
                    {
                        result.Add(item);
                    }
                }
                await Task.Delay(TimeSpan.FromSeconds(1));
                pageNum++;
            }
            return result;
        }

        protected abstract Task<ICollection<ICommentItem>> GetCommentByPage(long productId, int pageNum);
    }

    internal class VmallCollector : CollectorBase
    {
        internal class CommentItem: ICommentItem
        {
            public long Id => CommentId;

            public long CommentId { get; set; }

            public string CommentLevel { get; set; }

            public string Content { get; set; }

            public string SkuCode { get; set; }

            public string SkuName { get; set; }

            public long ProductId { get; set; }

            public int IsAnonymous { get; set; }
        }

        internal class CommentData
        {
            public CommentItem[] Comments { get; set; }
        }

        internal class Response<T>
        {
            public T Data { get; set; }

            public string ResultCode { get; set; }
        }

        protected override async Task<ICollection<ICommentItem>> GetCommentByPage(long productId, int pageNum)
        {
            var commentUrl = $"https://openapi.vmall.com/rms/v1/comment/getCommentList?pid={productId}&gbomCode=&extraType=&pageSize={PAGE_SIZE}&pageNum={pageNum}&sortType=1&showStatistics=false";
            var resp = await HttpGet(commentUrl);
            resp.EnsureSuccessStatusCode();
            var r = await resp.Content.ReadAsStringAsync();
            var commentResponse = JsonSerializer.Deserialize<Response<CommentData>>(r, new JsonSerializerOptions()
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });

            var resultCode = commentResponse.ResultCode;
            if (resultCode != "0")
            {
                throw new InvalidOperationException("Request failed");
            }

            return commentResponse.Data.Comments;
        }
    }
    
    internal class HonorShopCollector: CollectorBase
    {
        protected override async Task<ICollection<ICommentItem>> GetCommentByPage(long productId, int pageNum)
        {
            var commentUrl = $"https://openapi-cn.c.hihonor.com/rms/comment/getCommentList.json?t={DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()}";
            var resp = await HttpPost(commentUrl, new
            {
                pid = productId.ToString(),
                gbomCode = string.Empty,
                extraType = "1",
                pageSize = PAGE_SIZE,
                pageNum = pageNum
            });

            resp.EnsureSuccessStatusCode();
            var r = await resp.Content.ReadAsStringAsync();
            var commentResponse = JsonSerializer.Deserialize<VmallCollector.Response<VmallCollector.CommentData>>(r, new JsonSerializerOptions()
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });

            var code = commentResponse.ResultCode;
            if (code != "0")
            {
                throw new InvalidOperationException("Request failed");
            }

            return commentResponse.Data.Comments;
        }
    }

    internal class MiShopCollector: CollectorBase
    {
        public MiShopCollector(): base()
        {
            _httpClient.DefaultRequestHeaders.Add("Referer", "https://www.mi.com/");
        }

        public override async Task<ICollection<ICommentItem>> Collect(string url)
        {
            throw new NotImplementedException();
        }

        protected override async Task<ICollection<ICommentItem>> GetCommentByPage(long productId, int pageNum)
        {
            var commentUrl = $"https://api2.service.order.mi.com/user_comment/get_list?goods_id={productId}&v_pid={productId}&order_by=23&page_index={pageNum}&page_size={PAGE_SIZE}&profile_id=0&show_img=0&session_id=400b6924-7ed3-46a3-8887-3c66b577fc7b&callback=__jp8";
            var resp = await HttpGet(commentUrl);
            resp.EnsureSuccessStatusCode();
            var r = await resp.Content.ReadAsStringAsync();
            throw new NotImplementedException();
        }
    }

    internal class OppoShopCollector: CollectorBase
    {
        internal class Response<T>
        {
            public int Code { get; set; }

            public T Data { get; set; }
        }

        internal class GoodsDetail
        {
            public int GoodsSpuId { get; set; }

            public int GoodsSkuId { get; set; }

            public string GoodsSpuName { get; set; }
        }

        internal class CommentItem: ICommentItem
        {
            public long Id { get; set; }

            public int Star { get; set; }

            public string Content { get; set; }
        }

        internal class CommentData
        {
            public CommentItem[] Datas { get; set; }
        }

        public override async Task<ICollection<ICommentItem>> Collect(string url)
        {
            await HttpGet(url);

            _httpClient.DefaultRequestHeaders.Add("Referer", url);
            var productId = CommonExtractProductId(url);
            if (productId > 0)
            {
                var goodsDetailUri = $"https://www.opposhop.cn/cn/oapi/goods-detail/web/info/pc/sku?skuId={productId}&us=fenleiye";
                var resp = await HttpGet(goodsDetailUri);
                resp.EnsureSuccessStatusCode();
                var r = await resp.Content.ReadAsStringAsync();
                var goodsDetail = JsonSerializer.Deserialize<Response<GoodsDetail>>(r, new JsonSerializerOptions()
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                });
                var spuId = goodsDetail.Data.GoodsSpuId;
                return await GetComment(spuId);
            }

            return new List<ICommentItem>();
        }

        protected override async Task<ICollection<ICommentItem>> GetCommentByPage(long productId, int pageNum)
        {
            var commentUrl = $"https://www.opposhop.cn/cn/oapi/comment/web/getCommentListByCondition?type=0&desc=0&currentPage={pageNum}&pageSize={PAGE_SIZE}&tagIdsStr=&goodsSpuId={productId}";
            var resp = await HttpGet(commentUrl);
            resp.EnsureSuccessStatusCode();
            var r = await resp.Content.ReadAsStringAsync();
            var commentResponse = JsonSerializer.Deserialize<Response<CommentData>>(r, new JsonSerializerOptions()
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });

            var code = commentResponse.Code;
            if (code != 200)
            {
                throw new InvalidOperationException("Request failed");
            }

            return commentResponse.Data.Datas;
        }
    }

    internal class VivoShopCollector: CollectorBase
    {
        internal class Response<T>
        {
            public int Code { get; set; }

            public T Data { get; set; }
        }

        internal class RemarkItem: ICommentItem
        {
            public long Id => CreatedAt;

            public long CreatedAt { get; set; }

            public string Content { get; set; }

            public int SummaryScore { get; set; }
        }

        internal class CommentItem
        {
            public RemarkItem Remark { get; set; }
        }

        internal class CommentData
        {
            public CommentItem[] RemarkList { get; set; }
        }

        protected override async Task<ICollection<ICommentItem>> GetCommentByPage(long productId, int pageNum)
        {
            var commentUrl = $"https://shop.vivo.com.cn/api/v1/remark/getDetail?spuId={productId}&onlyHasPicture=false&lastPageNum={pageNum-1}&pageNum={pageNum}";
            var resp = await HttpGet(commentUrl);
            resp.EnsureSuccessStatusCode();
            var r = await resp.Content.ReadAsStringAsync();
            var commentResponse = JsonSerializer.Deserialize<Response<CommentData>>(r, new JsonSerializerOptions()
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });

            var code = commentResponse.Code;
            if (code != 0)
            {
                throw new InvalidOperationException("Request failed");
            }

            return commentResponse.Data.RemarkList.Select(x => x.Remark).ToArray();
        }
    }
}
